from neo4j import GraphDatabase as gd
from neobolt import exceptions as ex

'''
Credenciais 
uri = "bolt://localhost:7687"
userName = "neo4j"
password = "123456"
'''

class bancoNeo4j():
    def __init__(self, uri, userName, password):
        self.__driver  = gd.driver(uri, auth=(userName, password)) 

    def query(self, cmd):
        with self.__driver.session() as session:
            try:
                nodes = session.run(cmd)
                if nodes != None:
                    return nodes

            except ex.ConstraintError:
                return 0
            
            

'''           
bd = bancoNeo4j("bolt://localhost:7687", "neo4j", "123456")
#Criando categorias
bd.query("CREATE (:Categoria {nome: 'Higiene'}), (:Categoria {nome: 'Doces'}), (:Categoria {nome: 'Saúde'})")      
'''     



