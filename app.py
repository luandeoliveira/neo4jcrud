from PyQt5 import uic, QtCore
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import sys 
from neo4jbd import bancoNeo4j
from datetime import datetime

bd = bancoNeo4j("bolt://localhost:7687", "neo4j", "123456")

def mensagem(title, info):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setText(title)
    msg.setInformativeText(info)
    msg.exec()

class addProduto(QDialog):
    def __init__(self):
        super(addProduto, self).__init__()
        uic.loadUi('ui/addProduto.ui', self)  
        self.btnConfirmar.clicked.connect(self.salvarProduto)      
        self.show()

    def salvarProduto(self):        
        codigo = self.txtCodigoProduto.text()
        nome = self.txtNomeProduto.text()
        preco = self.txtPrecoProduto.text()
        validade = self.txtValidade.date().toString("yyyy.MM.dd")
        quantidade = self.txtQuantidade.text()
        query = f'CREATE (:Produto {{codigo: "{codigo}", nome: "{nome}", preco: "{preco}", validade: "{validade}", quantidade: "{quantidade}"}})'
        
        if bd.query(query) == 0:
            mensagem("Erro", "Produto já existe no banco")
        else:
            self.close()

class janelaPrincipal(QMainWindow):
    def __init__(self):
        super(janelaPrincipal, self).__init__()
        uic.loadUi('ui/principal.ui', self)
        self.cadastrarProduto.triggered.connect(lambda: self.abrirJanela(addProduto))       
        self.gerenciarProduto.triggered.connect(lambda: self.abrirJanela(gerenciarProduto))  
        self.cadastrarVenda.triggered.connect(lambda: self.abrirJanela(novaVenda)) 
        self.show()

    def abrirJanela(self, janela):                                            
        self.j = janela()
        self.j.show()
            

class gerenciarProduto(QMainWindow):
    def __init__(self):
        super(gerenciarProduto, self).__init__()
        uic.loadUi('ui/produto.ui', self)        
        self.tbProdutos.setColumnWidth(0, 100)
        self.tbProdutos.setColumnWidth(1, 300)
        self.tbProdutos.setColumnWidth(2, 100)
        self.tbProdutos.setColumnWidth(3, 100)
        self.tbProdutos.setColumnWidth(4, 100)
        self.tbProdutos.setSelectionBehavior(QTableView.SelectRows)       
        self.btnEditar.clicked.connect(self.editProduto)
        self.btnExcluir.clicked.connect(self.excluirProduto)
        self.completarTabela()
        
    def completarTabela(self):
        produtos = bd.query('MATCH (a:Produto) RETURN a.codigo, a.nome, a.preco, a.validade, a.quantidade') 
        
        for produto in produtos:
            linha = self.tbProdutos.rowCount()
            self.tbProdutos.insertRow(linha)
            self.tbProdutos.setItem(linha, 0, QTableWidgetItem(produto[0]))
            self.tbProdutos.setItem(linha, 1, QTableWidgetItem(produto[1]))
            self.tbProdutos.setItem(linha, 2, QTableWidgetItem(produto[2]))
            self.tbProdutos.setItem(linha, 3, QTableWidgetItem(produto[3]))
            self.tbProdutos.setItem(linha, 4, QTableWidgetItem(produto[4]))
        
                
    def excluirProduto(self):
        linha = self.tbProdutos.currentRow()
        codigo = self.tbProdutos.item(linha, 0).text()
        bd.query(f'MATCH (p:Produto {{codigo: "{codigo}"}}) DETACH DELETE p')
        self.tbProdutos.setRowCount(0)
        self.completarTabela()
        
      
    def editProduto(self): 
             
        #Instânciando janela                                    
        self.editJanela = editarProduto()
                       
        #Pegando valores do produto selecionado
        linha = self.tbProdutos.currentRow()
        codigo = self.tbProdutos.item(linha, 0).text()
        nome = self.tbProdutos.item(linha, 1).text()
        preco = self.tbProdutos.item(linha, 2).text()
        validade = self.tbProdutos.item(linha, 3).text()
        quantidade = self.tbProdutos.item(linha, 4).text()

        #Colocando valores nos textBoxes
        self.editJanela.txtCodigoProduto.setVisible(False);
        self.editJanela.txtNomeProduto.setText(nome)
        self.editJanela.txtPrecoProduto.setText(preco)
        self.editJanela.txtValidade.setDate(QtCore.QDate(datetime.strptime(validade, '%Y.%m.%d')))
        self.editJanela.txtQuantidade.setText(quantidade)

        self.editJanela.btnConfirmar.clicked.connect(lambda: salvar())

        def salvar():            
            nome = self.editJanela.txtNomeProduto.text()
            preco = self.editJanela.txtPrecoProduto.text()
            validade = self.editJanela.txtValidade.date().toString("yyyy.MM.dd")
            quantidade = self.editJanela.txtQuantidade.text()
            query = (
                f'MATCH (p:Produto {{codigo: "{codigo}"}})'
                f' SET p.nome = "{nome}", p.preco = "{preco}", p.validade = "{validade}", p.quantidade = "{quantidade}"'
            )
            bd.query(query)
            self.editJanela.close()
            self.tbProdutos.setRowCount(0)
            self.completarTabela()


        
       

class editarProduto(QDialog):
     def __init__(self):
        super(editarProduto, self).__init__()
        uic.loadUi('ui/addProduto.ui', self) 
        self.show()
        


class novaVenda(QMainWindow):
    def __init__(self):
        super(novaVenda, self).__init__()
        uic.loadUi('ui/venda.ui', self)    
        self.txtTotal.setText("0")  
        self.txtQuantidade.setText("1")  
        self.btnAdicionar.clicked.connect(self.addProduto)
        self.btnConfirmar.clicked.connect(self.confirmarCompra)
        self.produtosCarrinho = []
        self.show()

    def addProduto(self):
        codigo = self.txtCodBarra.text()

        produto = bd.query(f'MATCH (a:Produto {{codigo: "{codigo}"}}) RETURN a.codigo, a.nome, a.preco, a.validade, a.quantidade') 
        

        for pd in produto:          
                    
            linha = self.tbProdutos.rowCount()
            self.tbProdutos.insertRow(linha)
            self.tbProdutos.setItem(linha, 0, QTableWidgetItem(pd[0]))
            self.tbProdutos.setItem(linha, 1, QTableWidgetItem(pd[1]))
            self.tbProdutos.setItem(linha, 2, QTableWidgetItem(pd[2]))
            self.tbProdutos.setItem(linha, 3, QTableWidgetItem(self.txtQuantidade.text()))
            self.txtTotal.setText(str(float(self.txtTotal.text()) + float(pd[2])))
            self.produtosCarrinho.append(pd + tuple(self.txtQuantidade.text()))
            print(self.produtosCarrinho)
        
    def confirmarCompra(self):
        data = str(datetime.now())
        valor = self.txtTotal.text()
        cpf = self.txtCpfCliente.text()
        resultado = bd.query(f'CREATE (a:Venda {{data: "{data}", valor: {valor}}}) RETURN ID(a)')
        idVenda = 0
        
        for r in resultado:
            idVenda = r[0]

        for produto in self.produtosCarrinho:
            codigo = produto[0]
            valor = produto[2]
            quantidade = produto[5]
            quantidadeRestante = float(quantidade) - float(produto[4])            

            bd.query(
                f'MATCH (p:Produto {{codigo: "{codigo}"}})'
                f' MATCH (v:Venda) WHERE ID(v) = {idVenda}'            
                f' CREATE (v)-[:vendeu {{valor: {valor}, quantidade: {quantidade}}}]->(p)'              
                f' SET p.quantidade = "{quantidadeRestante}"'
            )

        bd.query(
            f'MATCH (c:Cliente {{cpf: "{cpf}"}})'
            f' MATCH (v:Venda) WHERE ID(v) = {idVenda}' 
            f' MATCH (u:Usuario {{nome: "Carlos"}})'
            f' CREATE (c)-[:comprou]->(v)'
            f' CREATE (u)-[:realizou]->(v)'
            
        )
        self.close()


        

        




if __name__ == "__main__":    
    app = QApplication(sys.argv)
    window = janelaPrincipal()
    sys.exit(app.exec_())


